public class HelloWorld {

    public static void main(String[] args) {
        String firstName = "Mike";
        String surname = "Simpson";

        String fullname = getFullName(firstName, surname);
        welcome(fullname);

        ifStatementCode();

        System.out.println("End of Program");
    }

    public static void printLine() {
        System.out.println("#####################");
    }

    public static String getFullName(String firstName, String surname) {
        return firstName + " " + surname;
    }

    public static void welcome(String name) {
        String output = "Hello Dr " + name + "!";
        printLine();
        System.out.println(output);
        printLine();
    }

    public static void ifStatementCode() {

        // Generating and initialising variable
        int x = 107;

        // Using if else statement
        if (x < 10) {
            System.out.println("x is less than 10");
        } else if (x <= 100) {
            System.out.println("x is between 10 and 100");
        } else {
            System.out.println("x is greater than 100");
        }

        // Using array, for loops and if statements to perform analysis on data set
        String[] animals = {"cat", "cat", "dog", "tortoise", "cat", "rabbit", "dog", "cat", "dog", "cat"};

        int numDogs = 0;
        int numCats = 0;

        for (String animal : animals) {
            if (animal.equals("cat")) {
                numCats++;
            } else if (animal.equals("dog")) {
                numDogs++;
            }
        }

        System.out.println("Number of cats: " + numCats);
        System.out.println("Number of dogs: " + numDogs);


        // Using arrays, for loops and if statements to find min and max ages in a dataset
        int[] ages = {24, 31, 29, 40, 18, 20, 42, 50};
        int minAge = ages[0];
        int maxAge = ages[0];

        for (int i = 1; i < ages.length; i++) {
            if (ages[i] > maxAge) {
                maxAge = ages[i];
            }
            if (ages[i] < minAge) {
                minAge = ages [i];
            }
        }

        System.out.println("Max age: " + maxAge);
        System.out.println("Min age: " + minAge);
    }

    public static void moduleOne() {
        // Additional Exercises
        System.out.println();
        System.out.println("Additional Exercise #1:");
        System.out.println("Hello Oliver!");

        // Twinkle Twinkle
        System.out.println();
        System.out.println("Additional Exercise #2:");
        System.out.println("Twinkle twinkle, little star,");
        System.out.println("How I wonder what you are.");
        System.out.println("Up above the world so high,");
        System.out.println("Like a diamond in the sky.");
        System.out.println("Twinkle twinkle, little star,");
        System.out.println("How I wonder what you are.");
    }

    public static void moduleTwo() {
        // Declaring and initialising variables
        int x = 10;
        int y = 17;

        // Operations on variables and concatenating strings and variables
        int z = x + y;
        System.out.println("z = " + z);

        // Incrementing variables
        z++;
        System.out.println("z = " + z);

        // Initialising variables for array
        int numElements = 10;
        int startingNumber = 1;
        int increment = 1;
        int multiple = 3;

        // Initialising array
        int[] numbers = new int[numElements];
        int currentNumber = startingNumber;

        // Using for loop to assign current integer to current place in array
        for (int i = 0; i < numElements; i++) {
            numbers[i] = currentNumber;
            currentNumber += increment;
        }

        // Modifying each element in the array
        for (int i = 0; i < numElements; i++) {
            numbers[i] *= multiple;
        }

        // Printing out all values in the array
        for (int i = 0; i < numElements; i++) {
            System.out.println(numbers[i]);
        }
    }


}
